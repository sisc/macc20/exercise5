
Started ddCOSMO_full_dense.
===========================

Input parameters:
-----------------
  molecule=caffeine, lebedev=110, lmax=6.

molecule = 

  struct with fields:

                   R: [1x24 double]
                   x: [24x3 double]
    chemical_element: 'CCNCONCONNCCCCHHHHHHHHHH'
              charge: [6 6 7 6 8 7 6 8 7 7 6 6 6 6 1 1 1 1 1 1 1 1 1 1]
                   M: 24
                  nl: {1x24 cell}

  fixed parameters: solvent permittivity epsilon_s = 78.4 for water

Running solver:
---------------
  building linear system LX=g...done.
  solving linear system LX=g...
gmres(588) converged at outer iteration 1 (inner iteration 35) to a solution with relative residual 8.7e-07.
  ...done.

Solution:
---------
Solver output statistics:
  length(X)=1176, filling factor(L)=0.120, sum(X)=-1.350e+02, sum(g)=-3.539e+02
  time_build_system=77.560 s, time_solve_system=0.056 s
Solvation energy:
  E_solvation_ref_2013a = -1.305e+03 au
  E_solvation_ref_2018_naive = 9.361e+01 au

Finished ddCOSMO_full_dense.
============================
