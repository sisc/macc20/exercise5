Started ddCOSMO_full_dense.
===========================

Input parameters:
-----------------
  molecule=benzene, lebedev=6, lmax=1.

molecule = 

  struct with fields:

                   R: [1x12 double]
                   x: [12x3 double]
    chemical_element: 'CCCCCCHHHHHH'
              charge: [6 6 6 6 6 6 1 1 1 1 1 1]
                   M: 12
                  nl: {1x12 cell}

  fixed parameters: solvent permittivity epsilon_s = 78.4 for water

Running solver:
---------------
  building linear system LX=g...done.
  solving linear system LX=g...
gmres(24) converged at outer iteration 1 (inner iteration 18) to a solution with relative residual 5.3e-07.
  ...done.

Solution:
---------
Solver output statistics:
  length(X)=48, filling factor(L)=0.158, sum(X)=-4.729e+01, sum(g)=-2.217e+02
  time_build_system=0.394 s, time_solve_system=0.071 s
Solvation energy:
  E_solvation_ref_2013a = -3.031e+02 au
  E_solvation_ref_2018_naive = 4.505e+00 au

Finished ddCOSMO_full_dense.
============================
