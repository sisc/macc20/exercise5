% Driver code for ddCOSMO_full_dense() solver.
%
%   Description:
%   Run ddCOSMO_full_dense() with a given molecule.
%
%   IMPORTANT NOTE:
%   - I changed provided code ylmbas.m, Geom_readPDB.m 
%     and Geom_read_Molec.m
%     to make usage more convenient. Use the versions here with this code.
%
%   IMPORTANT NOTE:
%   - This version was adapted after original exercise submission,
%     to allow comparison to the posted solution code, provided 
%     thereafter. See comments below.
% 
%   Usage:
%   - development only.
%

clc % clear command window
clear all % Remove items from workspace, freeing up system memory
% close all % Remove specified figure


%% constants

% available molecules FileType.MOL (= using Geom_Read_Molecule.m)
% NOTE: molecules with same name have different data than the PDB ones!
% NOTE: the solution code posted aftere exercise submission uses type MOL,
%       and not PDB! So use this one for result comparison.
molecule_name_list_mol = ["2Spheres" ...
                          "3Spheres" ...
                          "4Spheres" ...
                          "Ncube" ...
                          "HF" ...
                          "Formaldehyde" ...
                          "Benzene" ...
                          "Caffeine" ...
                          ];

% available molecules, FileType.PDB (= using Geom_ReadPDB.m)
% NOTE: molecules with same name have different data than the MOL ones!
molecule_name_list_pdb = ["101M" ...        %  1
                          "1B17" ...         %  2
                          "1CRN" ...         %  3 
                          "1ETN" ...         %  4 
                          "1GZI" ...         %  5 
                          "1YJO" ...         %  6 
                          "2spheres" ...     %  7 
                          "3DIK" ...         %  8 
                          "3spheres_2" ...   %  9 
                          "3spheres" ...     % 10 
                          "benzene_orig" ... % 11 
                          "benzene" ...      % 12
                          "caffeine" ...     % 13
                          "cube" ...         % 14
                          "lactose_1e2a" ... % 15
                          "Methanal" ...     % 16
                          ];


%% user input parameters

% molecule selection:
filetype = Filetype.MOL; % Filetype.PDB
molecule_name = "Caffeine"; % PDB version: "caffeine"

% lebedev quadrature degree:
% % recommended values for classical/MM code (ref. [2018] Table1, p.5/15):
% % lebedev_degree = 110, lmax = 6.
% %
% % allowed degrees: 6, 14, 26, 38, 50, 74, 86, 110, 146, 170, 194, 230, 266, 302, 
% % 350, 434, 590, 770, 974, 1202, 1454, 1730, 2030, 2354, 2702, 3074, 
% % 3470, 3890, 4334, 4802, 5294, 5810.
% % 
% % However, we are using the switch-case below, taken from the 
% % provided solution code after the exercise submission.
Disc_version = 1;
switch(Disc_version)
    % note: cases1-4 are identical to cases1-4 in
    % provided solution code.
    case 1
        % Standard
        lebedev_degree = 110; % gorder=17, compare solution InitData.m
        lmax = 6;
    case 2 
        % QM
        lebedev_degree = 302; % gorder=20
        lmax = 10;
    case 3
        % Max
        lebedev_degree = 1202; % gorder=59
        lmax = 20;
    case 4
        % "Exact"
        lebedev_degree = 5810; % gorder=131
        lmax = 50;
    case 5
        % "testing1_noHarmonics"
        % NOTE for comparison with solution code: solution code's
        % InitData.m automatically resets minimal lebedev_degree to 50,
        % if gorder < 11 (i.e., in {3,5,7,9}.) So there is no comparison
        % with unchanged solution code possible for this case.
        lebedev_degree = 6; % gorder=3
        lmax = 0;
    case 6
        % "testing2_singleHarmonic"
        % NOTE for comparison with solution code: solution code's
        % InitData.m automatically resets minimal lebedev_degree to 50.
        % if gorder < 11 (i.e., in {3,5,7,9}.) So there is no comparison
        % with unchanged solution code possible for this case.
        lebedev_degree = 6; % gorder=3
        lmax = 1;
end

% % input parameters used for report, before solution code was posted:
% % molecules: 2spheres 3spheres 3spheres_2 benzene caffeine.
% % (lebedev_degree,lmax): (6,0), (6,1), (6,2), (110,6).


%% run solver

[E_solvation, X] = ddCOSMO_full_dense(filetype, molecule_name, lebedev_degree, lmax);