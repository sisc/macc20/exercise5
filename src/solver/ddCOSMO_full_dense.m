function [E_solvation_ref_2013a, X] = ddCOSMO_full_dense(filetype, molecule_name, lebedev_degree, lmax)
%FULL_DENSE Compute solvation energy with ddCOSMO, LX=g built dense.
% 
%   Arguments:
%   molecule_name: basename of molecule PDF file (see load_molecule()).
%   lebedev_degree: Lebedev quadrature degree (see getLebedevSphere()).
%   lmax: lmax for spherical harmonics orthogonal basis (see ylmbas()).
%   
%   Follows the references [2013a], [2013b], [2018] (see README).

fprintf("Started ddCOSMO_full_dense.\n");
fprintf("===========================\n\n");

fprintf("Input parameters:\n");
fprintf("-----------------\n");
fprintf("  molecule=%s, lebedev=%i, lmax=%i.\n", ...
        molecule_name, lebedev_degree, lmax);
molecule = load_molecule(filetype, molecule_name);    
molecule
fprintf("  fixed parameters: solvent permittivity epsilon_s = 78.4 for water\n");

fprintf("\nRunning solver:\n");
fprintf("---------------\n");
% build linear system LX=g (L built as dense, returned as sparse matrix)
fprintf("  building linear system LX=g...");
tic;
[L,g] = build_LXg_dense(molecule, lebedev_degree, lmax);
time_build = toc;
fprintf("done.\n")

% solve linear system
fprintf("  solving linear system LX=g...\n");
tic;
restart = floor(length(L) / 2);
X = gmres(L,g,restart);
time_solve = toc;
fprintf("  ...done.\n\n")

% % test output of X, g
% X
% g

% compute solvation energy.
% - Assume interfacing with classical code for the solute, see ref. [2018] 
%   (see README) section 3.4, p.8/15.
% - Assume solvent is water.

% scaling factor for solvation energy
epsilon_s_H2O = 78.4; % ref [2013b] p. 8/15
f = solvation_energy_scaling_factor(epsilon_s_H2O);

% compute solvation energy according to ref. [2013a] (see README), eqn. (21) 
% first need to extract the (l,m)=(0,0) subvector from X
b = (lmax+1)^2; % atom i (l,m)-block side length in X
X_00 = X(1:b:end);
E_solvation_ref_2013a = sqrt(pi) * f * dot(molecule.charge, X_00);

% compute solvation energy according to ref. [2018] (see README), 
% p. 3/15, eqn. E_s^N. Naive: assume [Psi_i]_l^m = q_i from PDB.
dotprod = 0;
for i=1:molecule.M
    for l=1:lmax
        for m=-l:l
            brow = (l^2+1) + (m+l); % block-inner row index
            row = (i-1)*b + brow; % system row index              
            dotprod = dotprod + molecule.charge(i) * X(row);
        end % m
    end % l
end % i
E_solvation_ref_2018_naive = 1/2 * f * dotprod;

fprintf("Solution:\n");
fprintf("---------\n");
fprintf("Solver output statistics:\n");    
fprintf("  length(X)=%i, filling factor(L)=%.3f, sum(X)=%.3d, sum(g)=%.3d\n", ...
        length(X), nnz(L)/prod(size(L)), sum(X), sum(g));
fprintf("  time_build_system=%.3f s, time_solve_system=%.3f s\n",time_build, time_solve);
fprintf("Solvation energy:\n");
fprintf("  E_solvation_ref_2013a = %.3d au\n", E_solvation_ref_2013a);
fprintf("  E_solvation_ref_2018_naive = %.3d au\n", E_solvation_ref_2018_naive);

% % DEV test output
% fprintf("\n\nL");
% full(L)


fprintf("\nFinished ddCOSMO_full_dense.\n");
fprintf("============================\n");

% end % ddCOSMO_fullDense
