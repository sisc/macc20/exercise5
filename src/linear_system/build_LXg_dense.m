function [L,g] = build_LXg_dense(molecule, lebedev_degree, lmax)
% %FULL_DENSE Build ddCOSMO lin.sys. LX=g, L dense, returned sparse
% % 
% %   Arguments:
% %   molecule: a molecule
% %   leb_degree: Lebedev quadrature degree (i.e. resolution)
% %   lmax: lmax for spherical harmonics orthogonal basis

% build neighbors lists if not yet present
if ~isfield(molecule, 'nl')
    molecule = neighbors(molecule);
end

% get lebedev quadrature points
[lebx, lebw] = convert_lebedev(getLebedevSphere(lebedev_degree));
lebn = length(lebx);

% determine matrix L size parameters
M = molecule.M; % matrix side length in blocks
b = (lmax+1)^2; % block side length
rows = M*b;
cols = rows;

% initialize dense matrix L
L = zeros(rows);

% initialize right-hand side (RHS) g
g = zeros(rows,1);

% initialize i-j-only dependent tmp parameters 
% (Y = a spherical harmonics basis)
wij = zeros(lebn,1);
r = zeros(lebn, 1);
Y_lc_mc_arg = zeros(lebn, 3);
Y_lc_mc = zeros(lebn, b);
Y_lr_mr = zeros(lebn, b);
for n=1:lebn
    % Y_lr_mr is independent of any atom position
    Y_lr_mr(n,:) = ylmbas(lmax, lebx(n,:));
end    
pi4 = 4*pi;

% build dense L matrix
% i,j are the block indices. each block con
for i=1:M
    % precompute i-only-dependent atom centers xi, mapped to
    % unity sphere S^2, for later access
    xi = zeros(lebn, 3);
    for n=1:lebn
        xi(n,:) = molecule.x(i,:) + molecule.R(i) * lebx(n,:);
    end
    
    % build right hand-side g, elements [g_i]_{lc,mr}
    for lr=0:lmax
        for mr=-lr:lr
            
            % (matrix = global, block = local)
            brow = (lr^2+1) + (mr+lr); % block-inner row index
            mrow = (i-1)*b + brow; % matrix row index  
            
            % notice that we use brow below also to access the 
            % (lr,mr)-th value in the spherical harmonics basis Y.            
            
            sum = 0;
            % sum over Lebedev quadrature points
            for n=1:lebn
                Ui = 1 - weight_sum(molecule,i,xi(n,:));
                phi_vac = free_space_potential(molecule, xi(n,:));
                sum = sum + lebw(n) * Ui * phi_vac * Y_lr_mr(n,brow);
            end
            g(mrow) = -sum;
        end % mr
    end % lr            
    
    
    for j=1:M
        if i==j
            % compute element [L_ii]_{lr,mr}^{lc,mc}, i!=j
            
            % we are now in a block that lies on the matrix diagonal.
            % That block itself is diagonal. So we can compute it by
            % just looping over the block's diagonal.
            
            % loop over diagonal in that i,j-block in simple 1-increment.
            % (matrix = global, block = local)
            mrow_start = (i-1)*b + 1; % matrix row/col of 1st diag elmt in that block
            mrow_end = (i*b); % matrix row/col of last diag elmt in that block
            brow_minus1 = 0; % block-inner row/col index minus 1
            for mrow=mrow_start:mrow_end
                % compute lc by index inversion
                lc = floor(sqrt(brow_minus1));
                brow_minus1 = brow_minus1 + 1;
                
                L(mrow,mrow) = pi4 / (2*lc+1);
            end % row
            continue % j
        end % i==j
        
        % now we are in an off-diagonal i,j-block, i!=j
        
        % if atom j is not in atom i's neighbors list, then this whole 
        % block is zero. so if that is the case, we can skip it.
        if ~any(ismember(molecule.nl{i},j))
            continue
        end
        
        % now we are in an off-diagonal i,j-block, i!=j,
        % where atom j IS in atom i's neighbors list.
        % blocks like this are dense blocks.
                
        % precompute i-j-only-dependent tmp parameters
        % sum over Lebedev quadrature points
        for n=1:lebn
            distance = norm(xi(n,:) - molecule.x(j,:));
            normalization = norm(distance);

            wij(n) = weight(molecule, i,j, xi(n,:));
            r(n) = (normalization / molecule.R(j));
            Y_lc_mc_arg(n) = distance / normalization;
            Y_lc_mc(n,:) = ylmbas(lmax, Y_lc_mc_arg(n,:));
            % Y_lr_mr(n,:): already computed outside loop.
            
        end        
        
        % compute element [L_ij]_{lr,mr}^{lc,mc}, i!=j
        for lr=0:lmax
            for mr=-lr:lr
                for lc=0:lmax
                    for mc=-lc:lc
                        % fprintf("%3i %3i | %2i %2i | %3i %3i %3i %3i\n", row,col, i,j, lr,mr, lc,mc);

                        % (matrix = global, block = local)
                        brow = (lr^2+1) + (mr+lr); % block-inner row index
                        bcol = (lc^2+1) + (mc+lc); % block-inner col index
                        mrow = (i-1)*b + brow; % matrix row index
                        mcol = (j-1)*b + bcol; % matrix col index
                        
                        % notice that we use brow and bcol below also to
                        % access the (lr,mr)-th and (lc,mc)-th value in 
                        % the spherical harmonics basises Y.

                        sum = 0;
                        % sum over Lebedev quadrature points
                        for n=1:lebn
                            % use r(n)^lc, NOT r(n)!
                            sum = sum + lebw(n) * wij(n) * r(n)^lc ...
                                  * Y_lc_mc(n,bcol) * Y_lr_mr(n,brow);
                        end
                        L(mrow,mcol) = (-pi4 / (2*lc+1)) * sum;

                    end %mc
                end % lc
            end % mr
        end % lr
    end % j
end % i

% L
L = sparse(L);

end

