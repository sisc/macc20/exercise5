function [s, w] = convert_lebedev(lebedev)
%CONVERT_LEBEDEV Convert output of getLebedevSphere
%   converts output (struct with n fields x, y, z, w) to more useful
%   form: 3xn array of s and 1xn array of w (s stands for point on unit
%   sphere S^2).

s = zeros(lebedev.n, 3);
w = zeros(lebedev.n, 1);

for i=1:lebedev.n
    s(i,:) = [lebedev.x(i) lebedev.y(i) lebedev.z(i)];
    w(i) = lebedev.w(i);
end

end

