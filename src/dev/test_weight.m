% Summary: test of weight() function on simple molecule.
%
%   Description:
%   Test weight(molecule, i, j, x) function on simple molecule.
%   Test cases: 
%   "2spheres":
% 
%   Usage:
%   none
%

clc % clear command window
clear all % Remove items from workspace, freeing up system memory
% close all % Remove specified figure

leb_degree = 6;
leb = getLebedevSphere(leb_degree);
n_list = {};
for n=1:leb_degree
    n_list = [n_list n];
end
% xin = xi + Ri * sn, where
% i = 1, n = 4
% x1 = [0 0 0], R1 = 1.32, sn = (0 -1 0). So,
% xin = [0 -R1 0]
% This point is the only one of the 6 lebedev(6) point on ball Omega1
% which intersects with ball Omega2. As such, computing weights with
% all n=1,..6 lebedev points should return zero, except for n=4.

% test_molecules = ["2spheres", "3spheres", "3spheres_2", "benzene", "caffeine"];
test_molecules = ["2spheres", "3spheres", "3spheres_2", "benzene"];
tab = "  "; % "\t"

for basename = test_molecules
    molecule = load_molecule(basename, Filetype.PDB);
    fprintf("==============================================================\n")
    fprintf("molecule %s, M=%i atoms:\n", basename, molecule.M);
    fprintf("testing of weights(i,j,x) at Lebedev(degree=%i) points s_n,\n",leb_degree);
    fprintf("mapped to atoms as x_n^i := x_i + R_i * s_n; n = 1,...,%i.\n",leb_degree);
    fprintf("==============================================================\n")        

    molecule
    
    fprintf("--------------------------------------------------------------\n")
    fprintf("check whether all neighborhood weights w_ij(x_n^i) for atom i\n")
    fprintf("(j in neighborhood N_i) sum up correctly to either 0 or 1.\n")
    fprintf("--------------------------------------------------------------\n")  
    fprintf("%sn:     ",tab);
    disp(n_list)    
    fprintf("----------\n");
    for i=1:molecule.M
        wsum_list = {};
        for n=1:leb_degree
            sn = [leb.x(n) leb.y(n) leb.z(n)];
            xin = molecule.x(i,:) + molecule.R(i) * sn;
            wsum = weight_sum(molecule,i,xin);
            wsum_list = [wsum_list wsum];
        end % n
        fprintf("%si = %2i:",tab,i)        
        disp(wsum_list)        
    end % i    
    
    fprintf("--------------------------------------------------------------\n")
    fprintf("check whether all weights w_ij(x_n^i) for atom pairs i,j have\n")
    fprintf("have correct values in {0, 1, 1/2, 1/3, ...}. Meaning, atom i\n")
    fprintf("has zero, one, two, ... neighbors at that point and !=0 if\n")
    fprintf("atom j is one of them.\n")
    fprintf("--------------------------------------------------------------\n")    
    fprintf("%sn               ",tab);
    disp(n_list)
    fprintf("\n");
    for i=1:molecule.M
        for j=1:molecule.M
            if i == j
                continue
            end
            w_list = {};
            for n=1:leb_degree
                sn = [leb.x(n) leb.y(n) leb.z(n)];
                xin = molecule.x(i,:) + molecule.R(i) * sn;
                w = weight(molecule, i, j, xin);
                w_list = [w_list w];
            end % n
            fprintf("%s(i,j) = (%2i,%2i):",tab,i,j)            
            disp(w_list)
        end % j
    end % i
end



