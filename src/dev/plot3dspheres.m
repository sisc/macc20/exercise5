clc
clear
figure
[x,y,z] = sphere(20,1);
surf(x,y,z)  % sphere centered at origin
for i=1:5;
hold on
surf(x+randn(1,1),y+randn(1,1),z+randn(1,1))  % sphere centered at (3,-2,0)
surf(x+randn(1,1),y+randn(1,1),z+randn(1,1))  % sphere centered at (0,1,-3)
end
daspect([1 1 1])