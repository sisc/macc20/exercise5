% play around with traversal order of matrix L
% in LX = g, L dense, fully assembled.

clc % clear command window
clear all % Remove items from workspace, freeing up system memory
% close all % Remove specified figure


%constants
M=2;
lmax=1;

b = (lmax+1)^2;
rows = M*b;
cols = rows;

fprintf("==============================================================\n")
fprintf("Playing around with traversal order of matrix L in ddCOSMO LX=g\n");
fprintf("for full in-memory assembly of dense L with parameters\n");
fprintf("M=%i, lmax=%i ==> %i x %i matrix, block sidelength %i.\n",M,lmax,rows,cols,b);
fprintf("==============================================================\n")   


fprintf("\n\n");
fprintf("--------------------------------------------------------------\n")
fprintf("blockwise row-major traversal order (i,j,lr,mr,lc,mc):\n")
fprintf("(meaning row-major by block, row-major inside block)\n")
fprintf("-----------------------------------------\n")
fprintf("count row col |  i  j |  lr  mr  lc  mc\n")
fprintf("--------------+-------+------------------\n")
count = 0;
for i=1:M
    for j=1:M
        for lr=0:lmax
            for mr=-lr:lr
                for lc=0:lmax
                    for mc=-lc:lc
                        row = (i-1)*b + (lr^2+1) + (mr+lr);
                        col = (j-1)*b + (lc^2+1) + (mc+lc);
%                         count = (row-1)*cols + col; TODO: incorporate i,j
                        count = count + 1;
                        fprintf("\n%5i %3i %3i | %2i %2i | %3i %3i %3i %3i\n", count,row,col, i,j, lr,mr, lc,mc);
                    end
                end
            end
        end
    end
end

% fprintf("\n\n");
% fprintf("--------------------------------------------------------------\n")
% fprintf("traversal order (i,j,lr,lc,mr,mc):\n")
% fprintf("-----------------------------------------\n")
% fprintf("count row col |  i  j |  lr  mr  lc  mc\n")
% fprintf("--------------+-------+------------------\n")
% for i=1:M
%     for j=1:M
%         for lr=0:lmax
%             for lc=0:lmax
%                 for mr=-lr:lr
%                     for mc=-lc:lc
%                         row = (i-1)*b + (lr^2+1) + (mr+lr);
%                         col = (j-1)*b + (lc^2+1) + (mc+lc);
%                         count = (row-1)*cols + col;
%                         fprintf("\n%5i %3i %3i | %2i %2i | %3i %3i %3i %3i\n", count,row,col, i,j, lr,mr, lc,mc);
%                     end
%                 end
%             end
%         end
%     end
% end
% fprintf("seems like row, col, count calc. is not valid for this order.\n")