% Summary of this look_at_ylmbas: play with provided code.
%
%   Description:
%   Look at output of codes in provided/Geom.
% 
%   Usage:
%   none
%

clc % clear command window
% clear all % Remove items from workspace, freeing up system memory
% close all % Remove specified figure

fprintf("==============================================================\n")
fprintf("looking at output of Geom_ReadPDB(filename)\n")
fprintf("==============================================================\n")

basenames = ["2spheres", "3spheres", "3spheres_2", "benzene", "caffeine"];
molecule = load_molecule("2spheres", Filetype.PDB)

% print neighbor lists
fprintf(" i  E | neighbors\n")
fprintf("-----------------\n")
for i=1:molecule.M
    fprintf("%2i %2s | ", i, molecule.chemical_element(i))
    nl = molecule.nl{i};
    for j=1:length(nl)
        fprintf("%i, ", nl(j))
    end
    fprintf("\n")
end
