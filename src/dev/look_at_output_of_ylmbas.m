% Summary of this look_at_ylmbas: play with provided code.
%
%   Description:
%   Look at output of codes in provided/SphericalHarmonics.
% 
%   Usage:
%   none
%


clc % clear command window
% clear all % Remove items from workspace, freeing up system memory
% close all % Remove specified figure

fprintf("==============================================================\n")
fprintf("looking at output of Getvfac(lmax)\n")
fprintf("==============================================================\n")
for lmax = 1:3
    fprintf("lmax %i\n", lmax)
    % Getvfac(lmax): normalization factors for the spherical harmonics.
    % size 3x(lmax+1)^2 (l=0,...,lmax; m=-lmax,...,lmax).
    vfac = (Getvfac(lmax));
    fprintf(strjoin(["size vfac: ", num2str(size(vfac)), "\n"]))
    vfac
end

fprintf("\n\n\n")
fprintf("==============================================================\n")
fprintf("looking at output of getLebedevSphere(degree)\n")
fprintf("==============================================================\n")
for degree = [6, 14] % 26, 38, ...
    fprintf("----------\ndegree %i\n", degree)
    % Lebedev quadrature points. struct of x,y,z,w of sizes degreex1.
    leb = getLebedevSphere(degree)
    for n = 1:degree
        fprintf("x_%i = (%d, %d, %d), w_%i = %d  \n", ...
            n, leb.x(n), leb.y(n), leb.z(n), n, leb.w(n))
    end
    fprintf("\n")
end


fprintf("\n\n\n")
fprintf("==============================================================\n")
fprintf("looking at output of ylmbas(lmax,x)\n")
fprintf("==============================================================\n")
degree = 6
[leb_x, leb_w] = convert_lebedev(getLebedevSphere(degree));
fprintf("=========================\n")
fprintf("basis sizes\n")
fprintf("=========================\n")
for lmax=0:5
    fprintf("~~~~~~~~~~~~~~~~~~\nlmax=%i\n",lmax)    
   
    % ONB spherical harmonics basis for unit sphere.
    % size 1x(lmax+1)^2, ordered by incr. l,m ((0,0),(1,-1),(1,0),...).
    basis = ylmbas(lmax, [0,0,0]);
    fprintf(strjoin(["basis size: ", num2str(size(basis)), "\n"]))
end
fprintf("\n\n=========================\nbasis values for x\n=========================\n")
for n=1:degree
    fprintf("~~~~~~~~~~~~~~~~~~\nn=%i\n",n)  
    for lmax=0:2
        fprintf("-------------\nlmax=%i\n",lmax)    
        basis = ylmbas(lmax, leb_x(n,:))
    end
end

