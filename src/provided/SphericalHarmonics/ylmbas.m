function [basis,grad] = ylmbas(lmax,x,der)
% c
% c   calculates a basis of real spherical harmonics up to order lmax.
% c   the routine computes as a first the generalized legendre polynomials
% c   and then weighs them with sine and cosine coefficients.
% c   this lasts are evaluated starting from cos(phi) = y/sqrt(1-z**2)
% c   using chebyshev polynomials.
% c
% c   input:  lmax  ... maximum angular momentum of the spherical harmonics 
% c                     basis
% c           x     ... unit vector (x \in s_2) where to evaluate the basis
% c
% c   output: basis ... spherical harmonics at x. basis is dimensioned (lmax+1)^2
% c                     and the harmonics are ordered for increasing l and m 
% c                     (i.e.: 0,0; 1,-1; 1,0; 1,1; 2,-2; ...)
% c
% c   scratch: vplm ... scratch array dimensioned (lmax + 1)^2. it hosts the
% c                     generalized legendre polynomials.
% c            vcos ... scratch array dimensioned lmax + 1. it hosts the cos(m*\phi)
% c                     values
% c            vsin ... scratch array dimensioned lmax + 1. it hosts the sin(m*\phi)
% c                     values.
% c
 
% c
% c   get cos(\theta), sin(\theta), cos(\phi) and sin(\phi) from the cartesian
% c   coordinates of x.
% c

% vfac: scratch array dimensioned 2*nbasis. Contains the
% normalization factors for the spherical harmonics.
vfac = (Getvfac(lmax));

if(nargin==2)
    der = 0;    % derivative is not computed
end

Nx = size(x,1);
if(der)
    grad = zeros(Nx,3,(lmax+1)^2);
else
    grad = 0;
end


basis = zeros(Nx,(lmax+1)^2);
vcos = zeros(Nx,lmax+1);
vsin = zeros(Nx,lmax+1);
 
cthe = x(:,3);
sthe = sqrt(1 - cthe.*cthe);
 
sind = sthe~=0; 
sind_pos = size(x(sind,1),1) > 0;
if(sind_pos)
    cphi(sind,1) = x(sind,1)./sthe(sind);
    sphi(sind,1) = x(sind,2)./sthe(sind);
end
cphi(~sind,1) = 1;
sphi(~sind,1) = 0;
% c
% c   evaluate cos(m*phi) and sin(m*phi) arrays. notice that this is 
% c   pointless if z = 1, as the only non vanishing terms will be the 
% c   ones with m=0.
% c
if(sind_pos)
    [vc,vs] = trgev(lmax,cphi(sind),sphi(sind));
    vcos(sind,:) = vc;
    vsin(sind,:) = vs;
end
vcos(~sind,1:lmax + 1) = 1;
vsin(~sind,1:lmax + 1) = 0;
VC = 0;
VS = cthe(~sind);


% c
% c   evaluate the generalized legendre polynomials.
% c
vplm = polleg(lmax,cthe,sthe);
% c
% c   now build the spherical harmonics. we will distinguish m=0,
% c   m>0 and m<0:
% c

if(der)
    ethe = [cthe.*cphi,cthe.*sphi,-sthe];
    if(sind_pos)
        ephi(sind,:) = [-sphi(sind)./sthe(sind), cphi(sind)./sthe(sind), zeros(size(sphi(sind)))];
    end
    Nss = size(sthe(~sind),1);
    ephi(~sind,:) = [zeros(Nss,1),ones(Nss,1),zeros(Nss,1)];
end

for l = 0:lmax
    ind = l^2 + l + 1;
    % c       m = 0
    basis(:,ind) = vfac(1,ind)*vplm(:,ind);
    if(der)
        if(l==0)
            grad(:,:,ind) = zeros(Nx,3);
        else
            dPlm = vplm(:,ind+1);
            grad(:,:,ind) = vfac(1,ind)*ethe.*(dPlm*ones(1,3));
            
            % debug: only ephi-direction
            %grad(:,:,ind) = zeros(Nx,3);
        end
    end
    for m = 1:l
        plm = vplm(:,ind+m);
        % c         m > 0
        basis(:,ind+m) = vfac(1,ind+m)*plm.*vcos(:,m+1);
        % c         m < 0
        basis(:,ind-m) = vfac(1,ind+m)*plm.*vsin(:,m+1);
        
        
        if(der)
            if(m<l)
                dPlm = 0.5*((l+m)*(l-m+1)*vplm(:,ind+m-1) - vplm(:,ind+m+1));
            else
                dPlm = 0.5*(l+m)*(l-m+1)*vplm(:,ind+m-1);
            end
%             grad(:,:,ind+m) = vfac(1,ind+m)*(-ethe.*(dPlm.*vcos(:,m+1)*ones(1,3)) - m*ephi.*(plm.*vsin(:,m+1)*ones(1,3)));
%             grad(:,:,ind-m) = vfac(1,ind+m)*(-ethe.*(dPlm.*vsin(:,m+1)*ones(1,3)) + m*ephi.*(plm.*vcos(:,m+1)*ones(1,3)));
            if (size(x,1)==1)
                if (sind==1)
                 grad(sind,:,ind+m) = vfac(1,ind+m)*(-ethe(sind,:).*(dPlm(sind).*vcos(sind,m+1)*ones(1,3)) - m*ephi(sind,:).*(plm(sind).*vsin(sind,m+1)*ones(1,3)));
                 grad(sind,:,ind-m) = vfac(1,ind+m)*(-ethe(sind,:).*(dPlm(sind).*vsin(sind,m+1)*ones(1,3)) + m*ephi(sind,:).*(plm(sind).*vcos(sind,m+1)*ones(1,3)));
               elseif(sind==0)
               grad(~sind,:,ind+m) = vfac(1,ind+m)*(-ethe(~sind,:).*(dPlm(~sind).*vcos(~sind,m+1)*ones(1,3)) - ephi(~sind,:).*(dPlm(~sind).*VC*ones(1,3)));
               grad(~sind,:,ind-m) = vfac(1,ind+m)*(-ethe(~sind,:).*(dPlm(~sind).*vsin(~sind,m+1)*ones(1,3)) - ephi(~sind,:).*(dPlm(~sind).*VS*ones(1,3)));
                end 
           else 
            grad(sind,:,ind+m) = vfac(1,ind+m)*(-ethe(sind,:).*(dPlm(sind).*vcos(sind,m+1)*ones(1,3)) - m*ephi(sind,:).*(plm(sind).*vsin(sind,m+1)*ones(1,3)));
            grad(sind,:,ind-m) = vfac(1,ind+m)*(-ethe(sind,:).*(dPlm(sind).*vsin(sind,m+1)*ones(1,3)) + m*ephi(sind,:).*(plm(sind).*vcos(sind,m+1)*ones(1,3)));
            grad(~sind,:,ind+m) = vfac(1,ind+m)*(-ethe(~sind,:).*(dPlm(~sind).*vcos(~sind,m+1)*ones(1,3)) - ephi(~sind,:).*(dPlm(~sind).*VC*ones(1,3)));
            grad(~sind,:,ind-m) = vfac(1,ind+m)*(-ethe(~sind,:).*(dPlm(~sind).*vsin(~sind,m+1)*ones(1,3)) - ephi(~sind,:).*(dPlm(~sind).*VS*ones(1,3)));
            end
        

    end
    end
end 

% if(der)
%     disp('debug')
% end
% basis = x(:,3).^2*ones(1,(lmax+1)^2);
% if(der)
%     grad = zeros(Nx,3,(lmax+1)^2);
% %    grad(:,3,:) = ones(Nx,(lmax+1)^2);
%     grad(:,3,:) = 2*x(:,3)*ones(1,(lmax+1)^2);
% end

%%% Old version for single x evaluation
% basis = zeros((lmax+1)^2,1);
% %vplm = zeros((lmax+1)^2,1);
% vcos = zeros(lmax+1,1);
% vsin = zeros(lmax+1,1);
% 
% cthe = x(3);
% sthe = sqrt(1 - cthe*cthe);
% if(sthe~=0)
%     cphi = x(1)/sthe;
%     sphi = x(2)/sthe;
% else
%     cphi = 1;
%     sphi = 0;
% end
% % c
% % c   evaluate cos(m*phi) and sin(m*phi) arrays. notice that this is 
% % c   pointless if z = 1, as the only non vanishing terms will be the 
% % c   ones with m=0.
% % c
% if(sthe~=0)
%     [vcos,vsin] = trgev(lmax,cphi,sphi);
% else
%     for m = 1:lmax + 1
%       vcos(m) = 1;
%       vsin(m) = 0;
%     end
% end
% % c
% % c   evaluate the generalized legendre polynomials.
% % c
% vplm = polleg(lmax,cthe,sthe);
% % c
% % c   now build the spherical harmonics. we will distinguish m=0,
% % c   m>0 and m<0:
% % c
% for l = 0:lmax
%     ind = l^2 + l + 1;
%     % c       m = 0
%     basis(ind) = vfac(1,ind)*vplm(ind);
%     for m = 1:l
%         plm = vplm(ind+m);
%         % c         m > 0
%         basis(ind+m) = vfac(1,ind+m)*plm*vcos(m+1);
%         % c         m < 0
%         basis(ind-m) = vfac(2,ind-m)*plm*vsin(m+1);
%     end
% end