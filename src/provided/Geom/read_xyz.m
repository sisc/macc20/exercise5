function [Geom,Data] = read_xyz(filename)
% c
% c   Read xyz-file and save data to Geom
% c

scalingfactor = 1.1;

filename = ['data/',filename];
fid = fopen(filename,'r');

line = fgetl(fid);
it = 0;
while ischar(line)
    if it == 0
        Geom.M = sscanf(line,'%d');
        it = it+1;
        line = fgetl(fid);
        continue
    end
    
    [token,remain] = strtok(line); 
    c =  sscanf(remain,'%f %f %f');
    switch(token)
        %% UFF VdW radius table, Charges
            case  'H', r = 1.443; charge = 1;
            case  'C', r = 1.9255; charge = 6;
            case  'O', r = 1.75; charge = 8;
            case  'N', r = 1.83; charge = 7;
            case  'P', r = 2.0735; charge = 0;
            case  'S', r = 2.0175; charge = 0;
         otherwise, r = 1.75; charge = 0; line = fgetl(fid); continue;
    end
        
    Geom.R(it,1) = scalingfactor*r;
    Geom.centers(it,:) = c';
    Data.charges(it) = charge;
    
    it = it + 1;
    line = fgetl(fid);
end
it = it-1;
fclose(fid);
Geom.centers = Geom.centers(1:it,:);
Geom.M = it;
Data.x = Geom.centers;

end
