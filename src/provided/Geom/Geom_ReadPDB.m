function [Geom] = Geom_ReadPDB(filename)
% Read pdb-file and save data in Geom and rho

% filename = ['data/',filename,'.pdb'];
fid = fopen(filename,'r');

line = fgetl(fid);
it = 0;
while ischar(line)
	if(strncmp('HETATM',line,6) || strncmp('ATOM',line,4))
        it = it + 1;
        switch(line(14))
            case  'H', r = 1.2; charge = 1;
            case  'C', r = 1.7; charge = 6;
            case  'O', r = 1.52; charge = 8;
            case  'N', r = 1.55; charge = 7;
            otherwise, r = 1.0;
%             case  'H', color = [0.7 0.7 0.7]; r = 1.2; charge = 1;
%             case  'C', color = [0.3 0.3 1.0]; r = 1.7; charge = 6;
%             case  'O', color = [0.3 1.0 0.3]; r = 1.52; charge = 8;
%             case  'N', color = [1.0 0.3 1.0]; r = 1.55; charge = 7;
%             otherwise, color = [1.0 0.0 0.0]; r = 1.0;
        end
        
        c =  sscanf(line(31:54),'%f %f %f');
        
        Geom.R(it) = 1.1*r;
        Geom.x(it,:) = c;
        Geom.chemical_element(it) = line(14);
        Geom.charge(it) = charge;
 %       Data.color(it) = color;
        
    end
    line = fgetl(fid);
end
fclose(fid);
Geom.M = it;




