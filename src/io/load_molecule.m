function molecule = load_molecule(filetype, basename)
%SELECT_MOLECULE load a molecule from provided/Molecules
%   The list of availables molecules is in this function.
%   basename is the filename without extension.
%   currently only pdb files are supported.

%% constants
% available molecules FileType.MOL (= using Geom_Read_Molecule.m)
% NOTE: molecules with same name have different data than the PDB ones!
% NOTE: the solution code posted aftere exercise submission uses type MOL,
%       and not PDB! So use this one for result comparison.
basename_list_mol = ["2Spheres" ...
                      "3Spheres" ...
                      "4Spheres" ...
                      "Ncube" ...
                      "HF" ...
                      "Formaldehyde" ...
                      "Benzene" ...
                      "Caffeine" ...
                      ];

% available molecules, FileType.PDB (= using Geom_ReadPDB.m)
% NOTE: molecules with same name have different data than the MOL ones!
basename_list_pdb = ["101M" ...        %  1
                      "1B17" ...         %  2
                      "1CRN" ...         %  3 
                      "1ETN" ...         %  4 
                      "1GZI" ...         %  5 
                      "1YJO" ...         %  6 
                      "2spheres" ...     %  7 
                      "3DIK" ...         %  8 
                      "3spheres_2" ...   %  9 
                      "3spheres" ...     % 10 
                      "benzene_orig" ... % 11 
                      "benzene" ...      % 12
                      "caffeine" ...     % 13
                      "cube" ...         % 14
                      "lactose_1e2a" ... % 15
                      "Methanal" ...     % 16
                      ];
                      
basename_list_xyz = ["carbo" ...           %  1 
                    "hiv-1-gp41" ...       %  2
                    "l-plectasin" ...      %  3 
                    "ubch5b" ...           %  4 
                    "vancomycin" ...       %  5 
                    ];
                
basename_list_xyz_txt = ["glutaredoxin" ... %  1
                        ];    
molecule = [];                                        
switch filetype
    case Filetype.MOL
        if ~any(strcmp(basename_list_mol,basename))
            error(strjoin(["basename unknown. try one of: ", basename_list_mol]))
        end
        mol.name = basename;
        molecule = Geom_Read_Molec(mol);
        
    case Filetype.PDB
        if ~any(strcmp(basename_list_pdb,basename))
            error(strjoin(["basename unknown. try one of: ", basename_list_pdb]))
        end
        filename = strcat(basename, ".pdb");
        
        % read file
        % switch to current path to enable relative filepaths
        previous_path = pwd;
        current_path = fileparts(which(mfilename));
        cd(current_path);

        % 2sphere 7; 3sphere 9,10; benzene 11,12; caffeine 13.
        % note: 3sphere 9: no neighbors; benzene 11,12 identical.
        filepath = fullfile("..","provided","Molecules",filename);
        molecule = Geom_ReadPDB(filepath);

        % end, switch back path
        cd(previous_path);
    case Filetype.XYZ
        error("filetype XYZ currently not supported")
    case Filetype.XYZ_TXT
        error("filetype XYZ_TXT currently not supported")           
end

% compute neighbor lists
molecule = neighbors(molecule);   
                
end

