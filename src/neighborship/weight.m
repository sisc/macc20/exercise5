function omega = weight(molecule, i, j, x)
%WEIGHT compute weights for ddCOSMO 'interior' boundary condition
%   Description: ddCOSMO 'interior' boundary condition averages reaction
%   field potential W_i on vdW surface of atom_i as average over W_j's of
%   intersecting vdW balls at x. These are weighted according to number of
%   intersecting neighbors at x.
chi_j = is_in_ball(x, molecule.x(j,:), molecule.R(j));
denominator = 0;
for k = molecule.nl{i}
    denominator = denominator + is_in_ball(x, molecule.x(k,:), molecule.R(k));
  
end
if denominator == 0
    omega = 0;
else
    omega = chi_j / denominator;
end

