function is_in_ball = is_in_ball(x, center, radius)
%IS_IN_BALL Tells if x is in ball of radius around center.
%   Also called 'characteristic function' chi(x) of ball.
    d = x - center;
    d = norm(d);
    is_in_ball = d <= radius;
%     is_in_ball = ( norm(x - center) <= radius );
end

