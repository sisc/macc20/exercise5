function m = neighbors(m)
%NEIGHBORS computes neighborlist of atoms in molecule
%   neighboring atoms are those whose van der Waals
%   spheres intersect.
%   input: a molecule struct
%   output: molecule with field nl - neighbors list (cell array)

m.nl = {};
for i=1:m.M
    m.nl{i} = [];
end
for i=1:m.M
    for j=i+1:m.M
        d = norm(m.x(i,:) - m.x(j,:));
        if (d < (m.R(i)+m.R(j)))
            m.nl{i} = [m.nl{i}, j];
            m.nl{j} = [m.nl{j}, i];
        end
    end
end
end

