function sum = weight_sum(molecule, i, x)
%WEIGHT compute sum of atom i's neighbors' j weights present at point x
%   Description: See also weight(molecule, i, j, x).
%   weight_sum returns either 0 (no neighbors at x) or 1 (neighbors present
%   at x, all weighted such that sum to one).

sum = 0;
for j = molecule.nl{i}
    w = weight(molecule, i, j, x);
    sum = sum + w;
end % j neighbors
end

