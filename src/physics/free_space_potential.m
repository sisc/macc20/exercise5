function phi = free_space_potential(molecule, x)
%FREE_SPACE_POTENTIAL Compute vacuum potential of molecule at point x 
%   Ref. [2013a] (see README), eqn. (3). Equivalent to discretized version,
%   Phi_n^i, ref. [2013a], p.5/12. Equivalent to interfacing ddCOSMO with
%   a classical code for the solute, see ref. [2018] section 3.4 p.8/15, 
%   eqn. (32).

phi = 0;
for i=1:molecule.M
    phi = phi + molecule.charge(i) / norm(x - molecule.x(i,:));
end

end

