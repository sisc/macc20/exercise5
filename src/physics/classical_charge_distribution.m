function rho = classical_charge_distribution(molecule, x)
%CLASSICAL_CHARGE_DISTRIBUTION of a molecule
%   Ref. [2013a] (see README), p.2/12.
%   Attributes:
%   molecule: loaded with load_molecule()
%   x: a 1x3 position.

rho = 0
for i=1:molecule.M
    rho = rho + molecule.charge(i) * isequal(x, molecule.x(i,:))
end
end

