function f = solvation_energy_scaling_factor(epsilon_s)
%SCALING_FACTOR COSMO empirical function for computing solvation energy
%   Taken from ref. [2013b] p.3/15 (see README). epsilon_s is the 
%   relative permittivity = dielectric consant of the solvent, 78.4 for
%   water.

k = 1/2;
f = (epsilon_s - 1) / (epsilon_s + k);
end

