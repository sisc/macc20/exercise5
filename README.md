# exercise5

ddCOSMO solvation model, implementation in MATLAB

Course: Mathematical aspects in computational chemistry 2020.

Student: Johannes Wasmer, 090800.

# Usage #

The driver file is `src/main.m`. There the user can set input parameters and run the solver.

# Report #

See `report.pdf`.

# References

- [2013a] Fast Domain Decomposition Algorithm for Continuum Solvation Models: Energy and First Derivatives. URL https://pubs.acs.org/doi/10.1021/ct400280b
- [2013b] Domain decomposition for implicit solvation models. URL https://aip.scitation.org/doi/10.1063/1.4816767
- [2018] How to make continuum solvation incredibly fast in a few simple steps. URL https://onlinelibrary.wiley.com/doi/full/10.1002/qua.25669

